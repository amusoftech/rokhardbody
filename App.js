/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */

import React, { useEffect, useState ,useMemo} from 'react';
import {
  SafeAreaView,
  StyleSheet,
  ScrollView,
  View,
  Text,
  StatusBar,
  Platform,
} from 'react-native';

import {
  LearnMoreLinks,
  Colors,
  DebugInstructions,
  ReloadInstructions,
} from 'react-native/Libraries/NewAppScreen';
import Header from './app/components/header/Header.js';
import Search from './app/components/search/Search.js';
import Home from './app/screens/dashboard/Index.js';
import Video from './app/screens/dashboard/Video.js';
import Footer from './app/components/footer/Footer.js';
import {requestUserPermission} from './app/firebase/messaging.js';
import messaging from '@react-native-firebase/messaging';
import DropdownAlert from 'react-native-dropdownalert';
import {checkIfLoggedIn} from './app/firebase/login/login.js';
import {AlertHelper} from './app/components/AlertHelper/AlertHelper.js';
import {markIsWatched,isWatchedBefore} from './app/utils/FullScreenVideoHandler' 

const App: () => React$Node = () => {
   checkIfLoggedIn();
  requestUserPermission();
  useEffect(() => {
    const unsubscribe = messaging().onMessage(async remoteMessage => {
      const notification = remoteMessage.notification;
      console.log('notification', remoteMessage);
      if (Platform.OS == 'ios') {
        AlertHelper.show(
          'success',
          remoteMessage.data.notification.title,
          remoteMessage.data.notification.body,
        );
      } else {
        AlertHelper.show('success', notification.title, notification.body);
      } 
    });

    return unsubscribe;
  }, []);  

 

  const [showVIdeo, setshowVIdeo] = useState(false)
 useMemo(() => {
   const watched = isWatchedBefore();
   watched.then(value=>{
    console.log("watched >>",value);
    setshowVIdeo( value != null)
   }) 
    

 }, [])
  return (
    <>
      {!showVIdeo ?
        <Video  
        //AsyncStorage
        onDone={async ()=>{ 
          await markIsWatched()
          setshowVIdeo(!showVIdeo)
        }}
        onClick={ async () => {
          await markIsWatched()
          setshowVIdeo(!showVIdeo)
        }} />
        :
        <React.Fragment>
          <StatusBar barStyle="dark-content" />
          <SafeAreaView style={{ flex: 1 }}>
            <Header />
            <Search />
            <Home />
            <Footer />
            <DropdownAlert
              defaultContainer={{ padding: 25, paddingTop: StatusBar.currentHeight, flexDirection: 'row' }}
              ref={ref => AlertHelper.setDropDown(ref)}
              onClose={() => AlertHelper.invokeOnClose()}
            />
          </SafeAreaView>
        </React.Fragment>
      }
    </>
  );
};

const styles = StyleSheet.create({
  scrollView: {
    backgroundColor: Colors.lighter,
  },
  engine: {
    position: 'absolute',
    right: 0,
  },
  body: {
    backgroundColor: Colors.white,
  },
  sectionContainer: {
    marginTop: 32,
    paddingHorizontal: 24,
  },
  sectionTitle: {
    fontSize: 24,
    fontWeight: '600',
    color: Colors.black,
  },
  sectionDescription: {
    marginTop: 8,
    fontSize: 18,
    fontWeight: '400',
    color: Colors.dark,
  },
  highlight: {
    fontWeight: '700',
  },
  footer: {
    color: Colors.dark,
    fontSize: 12,
    fontWeight: '600',
    padding: 4,
    paddingRight: 12,
    textAlign: 'right',
  },
});

export default App;
