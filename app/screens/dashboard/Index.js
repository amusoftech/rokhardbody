import React, { Component } from 'react'
import {Linking,  Text, View,Image,ImageBackground,Dimensions,StyleSheet,TouchableOpacity } from 'react-native'
import { Color } from '../../utils/StyleGuide';


const {width,height}=Dimensions.get('window')
 const openWebsite = ()=>{
    Linking.openURL("https://rokhardbody.com/")
 }
const Home=()=> {

        return (
            <View style={{flex:1}}>
                         
                <Image source={require('../../assets/imgs/photo1.png')} style={{height:200,width:'100%'}} resizeMode='stretch'/>
                <View style={{flexDirection:'row',flexWrap:'wrap' }}>
                    <TouchableOpacity onPress={openWebsite} activeOpacity={0.5}>
                    <ImageBackground source={require("../../assets/imgs/suppliment.png")} style={styles.imageComp}>
                        <Text style={styles.label}>SUPPLEMENTS</Text>
                    </ImageBackground>
                    </TouchableOpacity>
                    <TouchableOpacity onPress={openWebsite}  activeOpacity={0.5}>
                    <ImageBackground source={require("../../assets/imgs/videoTraining.png")} style={styles.imageComp}>
                    <Text style={styles.label}>VIDEO TRAINING</Text>
                    </ImageBackground>
                    </TouchableOpacity>
                   <TouchableOpacity  onPress={openWebsite}  activeOpacity={0.5}>
                   <ImageBackground source={require("../../assets/imgs/audio.png")} style={styles.imageComp1}>
                    <Text style={styles.label}>AUDIO</Text>
                    </ImageBackground>
                   </TouchableOpacity >
                    <TouchableOpacity onPress={openWebsite}  activeOpacity={0.5}>
                    <ImageBackground source={require("../../assets/imgs/apparel.png")} style={styles.imageComp1}>
                    <Text style={styles.label}>APPAREL</Text>
                    </ImageBackground>
                    </TouchableOpacity>
                    <TouchableOpacity   onPress={openWebsite} activeOpacity={0.5}>
                    <ImageBackground source={require("../../assets/imgs/lifingShoe.png")} style={styles.imageComp1}>
                    <Text style={styles.label}>LIFTING SHOES</Text>
                    </ImageBackground>
                    </TouchableOpacity>
                </View>
            </View>
        )
    }

const styles=StyleSheet.create({
    imageComp:{height:150,
    width:width/2-20,
    marginHorizontal:10,
    justifyContent:'flex-end',
    alignItems:'center'
},
    imageComp1:{height:"75%",width:width/3,justifyContent:'flex-end',
    alignItems:'center'
},
    label:{color:Color.white,fontSize:20,fontWeight:'bold',marginBottom:10,textAlign:'center'}
})
export default Home