import React, { Component } from 'react'
import { Button, Text, View, StyleSheet, TouchableOpacity } from 'react-native';
import Video from 'react-native-video';
import { Color } from '../../utils/StyleGuide';
import Home from './Index.js';


export default class VideoPlayer extends Component {
  
  render() {
    const {onClick, onDone} = this.props;
    return (
      <View >
        <View style={{ height: '100%', width: '100%' }}>

          <Video onEnd={onDone} source={require('../../assets/Slide.mp4')}
            style={styles.backgroundVideo}
            resizeMode="cover"
            repeat={true}
          />
        </View>
        <View style={styles.btnStyle}>
          
          <Button
            // onPress={this.handleClick}   
            onPress={onClick}
            title="Skip"
            style={styles.label}
          />
        </View>
      </View>
    )
  }
}

var styles = StyleSheet.create({
  backgroundVideo: {
    position: 'absolute',
    top: 0,
    left: 0,
    bottom: 0,
    right: 0,
    height: '100%',
    width: '100%'
  },
  btnStyle: {
    position: 'absolute',
    right: 20,
    top: 20,
    justifyContent: 'flex-start',
    alignItems: 'flex-start'
  },
  label: {
    color: Color.black,
    fontSize: 20,
    fontWeight: 'bold',
    marginBottom: 10,
    textAlign: 'center',
    position: 'absolute',
    backgroundColor: '#00aeef',
    borderColor: 'red',

  }
});