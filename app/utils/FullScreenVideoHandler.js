import   AsyncStorage from '@react-native-community/async-storage'


export const  isWatchedBefore =async () =>{
    try {
        const value = await AsyncStorage.getItem('@isWatched') 
        return value;
      } catch(e) {
        // error reading value
      }
}
export const  markIsWatched = async () =>{
    try {
        await AsyncStorage.setItem('@isWatched', "YES")
      } catch (e) {
        // saving error
      }
}