import React, {Component} from 'react';
import {Text, View, TextInput, StyleSheet} from 'react-native';
import {Color} from '../../utils/StyleGuide';

export default class Search extends Component {
  render() {
    return (
      <View style={styles.container}>
        <TextInput
          placeholder="Search"
          style={styles.Search}
          returnKeyType="search"
        />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    height: 45,
    paddingHorizontal: 10,
    backgroundColor: Color.primary,
    paddingVertical: 5,
  },
  Search: {
    height: 30,
    backgroundColor: Color.white,
    borderRadius: 6,
    padding: 10,
    fontSize: 12,
    justifyContent: 'center',
  },
});
