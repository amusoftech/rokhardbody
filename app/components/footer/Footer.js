import React, { Component } from 'react'
import { Text, View,Image,StyleSheet,TouchableOpacity } from 'react-native'
import { Color } from '../../utils/StyleGuide'

export default class Footer extends Component {
    render() {
        return (
                <View style={{justifyContent:'center',alignItems:'center',backgroundColor:Color.black,height:60}}>
                <Image source={require("../../assets/icons/RHBLogo.png")} style={styles.logo}/>
                </View>
        )
    }
}

const styles=StyleSheet.create({
    container:{height:80,backgroundColor:Color.primary,flexDirection:'row',alignItems:'center',paddingHorizontal:10,justifyContent:'space-between'},
    leftIcon:{height:30,width:30},
    logo:{height:40,width:60},

})