import React, { Component } from 'react'
import { Text, View,Image,StyleSheet,TouchableOpacity } from 'react-native'
import { Color } from '../../utils/StyleGuide'

export default class Header extends Component {
    render() {
        return (
            <View style={styles.container}>
                <TouchableOpacity >
                <Image source={require("../../assets/icons/bars.png")} style={styles.leftIcon}/>
                </TouchableOpacity>
                <View style={{flex:1,justifyContent:'center',alignItems:'center',position:'absolute',left:0,right:0}}>
                <Image source={require("../../assets/icons/RHBLogo.png")} style={styles.logo}/>
                </View>
                <View style={{flexDirection:'row'}}>
                <TouchableOpacity>
                <Image source={require("../../assets/icons/users.png")} style={[styles.leftIcon,{marginHorizontal:25}]}/>
                </TouchableOpacity>
                <TouchableOpacity>
                <Image source={require("../../assets/icons/business.png")} style={styles.leftIcon}/>
                </TouchableOpacity>

                </View>
            </View>
        )
    }
}

const styles=StyleSheet.create({
    container:{height:80,backgroundColor:Color.primary,flexDirection:'row',alignItems:'center',paddingHorizontal:10,justifyContent:'space-between'},
    leftIcon:{height:30,width:30},
    logo:{height:50,width:75},

})