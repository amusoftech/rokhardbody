import firestore from '@react-native-firebase/firestore';
import auth from '@react-native-firebase/auth';

//console.log('auth().currentUser', auth().currentUser);

const userId = auth().currentUser && auth().currentUser.uid;
export const saveFcm = token => {
  if(token !=null ||token !=undefined && userId )
  firestore()
    .collection('deviceTokens')
    .doc(userId)
    .set({
      fcmToken: token,
    });
};
