import auth from '@react-native-firebase/auth';
import messaging from '@react-native-firebase/messaging';
import {saveFcm} from '../firestore/fcm';
export const checkIfLoggedIn = () => {
  auth().onAuthStateChanged(async function(user) {
    if (!user) {
        login();
    }  else {
      messaging()
        .getToken()
        .then(  token => {
          console.log("token>>",token);
          
         saveFcm(token);
        }); 
      messaging().onTokenRefresh(  token => {
        console.log("token>>",token);
          saveFcm(token);
      });
    }  
  });
};
const login = async () => {
  try {
    await auth().signInAnonymously();
  } catch (e) {
    switch (e.code) {
      case 'auth/operation-not-allowed':
        console.log('Enable anonymous in your firebase console.');
        break;
      default:
        console.error(e);
        break;
    }
  }
};
